# **Description** #
Identify future calendar events with date and time from text. Five events – Marriage, Birthday Party, Meeting Anniversary, Seminar will be included in scope.

## **Pre-requisites** ##
* Install Depeendency for mxDateTime package needed for Timex module
 	* pip install egenix-mx-base
 	* or download from http://www.egenix.com/products/python/mxBase/#Download